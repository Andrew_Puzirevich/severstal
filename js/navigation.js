/**
 * Created by Андрій Пузиревич on 14.02.16.
 */
function {
    document.getElementById('slidemain').style.display = 'block';
    document.getElementById('sendmail').style.display = 'none';
    document.getElementById('mailonsend').style.display = 'none';
    document.getElementById('adduser').style.display = 'none';
    document.getElementById('viewuser').style.display = 'none';
    document.getElementById('activuser').style.display = 'none';
    document.getElementById('viewdatabase').style.display = 'none';
    document.getElementById('contacts').style.display = 'none';
    document.getElementById('mailblock').style.display = 'none';
}

function sendmail(){
    document.getElementById('slidemain').style.display = 'none';
    document.getElementById('sendmail').style.display = 'block';
    document.getElementById('mailonsend').style.display = 'none';
    document.getElementById('adduser').style.display = 'none';
    document.getElementById('viewuser').style.display = 'none';
    document.getElementById('activuser').style.display = 'none';
    document.getElementById('viewdatabase').style.display = 'none';
    document.getElementById('contacts').style.display = 'none';
    document.getElementById('mailblock').style.display = 'none';
}

function mailonsend(){
    document.getElementById('slidemain').style.display = 'none';
    document.getElementById('sendmail').style.display = 'none';
    document.getElementById('mailonsend').style.display = 'block';
    document.getElementById('adduser').style.display = 'none';
    document.getElementById('viewuser').style.display = 'none';
    document.getElementById('activuser').style.display = 'none';
    document.getElementById('viewdatabase').style.display = 'none';
    document.getElementById('contacts').style.display = 'none';
    document.getElementById('mailblock').style.display = 'none';
}

function adduser(){
    document.getElementById('slidemain').style.display = 'none';
    document.getElementById('sendmail').style.display = 'none';
    document.getElementById('mailonsend').style.display = 'none';
    document.getElementById('adduser').style.display = 'block';
    document.getElementById('viewuser').style.display = 'none';
    document.getElementById('activuser').style.display = 'none';
    document.getElementById('viewdatabase').style.display = 'none';
    document.getElementById('contacts').style.display = 'none';
    document.getElementById('mailblock').style.display = 'none';
}

function viewuser(){
    document.getElementById('slidemain').style.display = 'none';
    document.getElementById('sendmail').style.display = 'none';
    document.getElementById('mailonsend').style.display = 'none';
    document.getElementById('adduser').style.display = 'none';
    document.getElementById('viewuser').style.display = 'block';
    document.getElementById('activuser').style.display = 'none';
    document.getElementById('viewdatabase').style.display = 'none';
    document.getElementById('contacts').style.display = 'none';
    document.getElementById('mailblock').style.display = 'none';
}

function activuser(){
    document.getElementById('slidemain').style.display = 'none';
    document.getElementById('sendmail').style.display = 'none';
    document.getElementById('mailonsend').style.display = 'none';
    document.getElementById('adduser').style.display = 'none';
    document.getElementById('viewuser').style.display = 'none';
    document.getElementById('activuser').style.display = 'block';
    document.getElementById('viewdatabase').style.display = 'none';
    document.getElementById('contacts').style.display = 'none';
    document.getElementById('mailblock').style.display = 'none';
}

function viewdatabase(){
    document.getElementById('slidemain').style.display = 'none';
    document.getElementById('sendmail').style.display = 'none';
    document.getElementById('mailonsend').style.display = 'none';
    document.getElementById('adduser').style.display = 'none';
    document.getElementById('viewuser').style.display = 'none';
    document.getElementById('activuser').style.display = 'none';
    document.getElementById('viewdatabase').style.display = 'block';
    document.getElementById('contacts').style.display = 'none';
    document.getElementById('mailblock').style.display = 'none';
}

function contacts(){
    document.getElementById('slidemain').style.display = 'none';
    document.getElementById('sendmail').style.display = 'none';
    document.getElementById('mailonsend').style.display = 'none';
    document.getElementById('adduser').style.display = 'none';
    document.getElementById('viewuser').style.display = 'none';
    document.getElementById('activuser').style.display = 'none';
    document.getElementById('viewdatabase').style.display = 'none';
    document.getElementById('contacts').style.display = 'block';
    document.getElementById('mailblock').style.display = 'none';
}

function postmail(){
    document.getElementById('slidemain').style.display = 'none';
    document.getElementById('sendmail').style.display = 'none';
    document.getElementById('mailonsend').style.display = 'none';
    document.getElementById('adduser').style.display = 'none';
    document.getElementById('viewuser').style.display = 'none';
    document.getElementById('activuser').style.display = 'none';
    document.getElementById('viewdatabase').style.display = 'none';
    document.getElementById('contacts').style.display = 'none';
    document.getElementById('mailblock').style.display = 'block';
}

function allgt(){
    document.getElementById('allgt').style.display = 'block';
    document.getElementById('allio').style.display = 'none';
    document.getElementById('allmo').style.display = 'none';
}

function allio(){
    document.getElementById('allgt').style.display = 'none';
    document.getElementById('allio').style.display = 'block';
    document.getElementById('allmo').style.display = 'none';
}

function allmo(){
    document.getElementById('allgt').style.display = 'none';
    document.getElementById('allio').style.display = 'none';
    document.getElementById('allmo').style.display = 'block';
}

function sendgt(){
    document.getElementById('sendgt').style.display = 'block';
    document.getElementById('onsendgt').style.display = 'none';
}

function onsendgt(){
    document.getElementById('onsendgt').style.display = 'block';
    document.getElementById('sendgt').style.display = 'none';
}

function sendio(){
    document.getElementById('sendgt').style.display = 'none';
    document.getElementById('sendio').style.display = 'block';
    document.getElementById('sendmo').style.display = 'none';
}

function sendmo(){
    document.getElementById('sendgt').style.display = 'none';
    document.getElementById('sendio').style.display = 'none';
    document.getElementById('sendmo').style.display = 'block';
}


function onsendio(){
    document.getElementById('onsendgt').style.display = 'none';
    document.getElementById('onsendio').style.display = 'block';
    document.getElementById('onsendmo').style.display = 'none';
}

function onsendmo(){
    document.getElementById('onsendgt').style.display = 'none';
    document.getElementById('onsendio').style.display = 'none';
    document.getElementById('onsendmo').style.display = 'block';
}

function onsendpropose(){
    document.getElementById('onsendpropose').style.display = 'block';
    document.getElementById('sendpropose').style.display = 'none';
}

function sendpropose(){
    document.getElementById('sendpropose').style.display = 'block';
    document.getElementById('onsendpropose').style.display = 'none';
}