<?php
/**
 * Created by PhpStorm.
 * User: Андрій Пузиревич
 * Date: 17.01.16
 * Time: 17:08
 */




$command = $_GET["cmd"];


if(strnatcasecmp($command,"send") == 0){

   $mail = $_POST["mail"];

   require_once("../admin/core.php");
   $DB = new DB;
   $DB->connect();
   $arr = array(
       "email"=>$mail,
       "type_tab"=>'Напоминание',
       "sendtrue"=>1
   );
   $DB->Insert('mail_order',$arr);
    echo "";
   echo json_encode($arr);
    session_start();
    $_SESSION['notice'] = "Напоминание";
    $_SESSION['noticemail'] = $mail;
    require_once "SendMailToSeverStal.php";
    require_once "SendTest.php";
}

if(strnatcasecmp($command,"givetz") == 0){
    $name = $_POST["name"];
    $mail = $_POST["mail"];
    $tel = $_POST["tel"];

    require_once("../admin/core.php");
    $DB = new DB;
    $DB->connect();
    $givetzarr = array(
        "name"=>$name,
        "email"=>$mail,
        "tel"=>$tel,
        "type_tab"=>'Есть проект'
    );
    $DB->Insert('give_task',$givetzarr);

    echo json_encode($givetzarr);
    session_start();
    $_SESSION['notice'] = "Есть готовый проект";
    $_SESSION['noticemail'] = $mail;
    $_SESSION['tzinfo'] = " Ф.И.О. заказчика " .$name. ", контактный телефон: " .$tel.".";
    require_once "SendMailToSeverStal.php";
}

if(strnatcasecmp($command,"calculation") == 0){
    $width = $_POST["width"];
    $long  = $_POST["long"];
    $height  = $_POST["height"];
    $lifting  = $_POST["lifting"];
    $company_name  = $_POST["company_name"];
    $country  = $_POST["country"];
    $city  = $_POST["city"];
    $email  = $_POST["email"];
    $lng  = $_POST["lng"];
    $cost1t = $_POST["cost1t"];
    $costmontag = $_POST["costmontag"];

    require_once("../admin/core.php");
    $DB = new DB;
    $DB->connect();
    $calculation = array(
        "width"=>$width,
        "lengh"=>$long,
        "height"=>$height,
        "lifting"=>$lifting,
        "company_name"=>$company_name,
        "country"=>$country,
        "city"=>$city,
        "email"=>$email,
        "lng"=>$lng,
        "cost1t"=>$cost1t,
        "costmontag"=>$costmontag
    );
    $DB->Insert('calculation',$calculation);

    require_once "Calculation.php";
}

if(strnatcasecmp($command,"givetzindivid") == 0){
    $name = $_POST["name"];
    $mail = $_POST["mail"];
    $tel = $_POST["tel"];
    $description = $_POST["description"];
    require_once("../admin/core.php");
    $DB = new DB;
    $DB->connect();
    $giveindividarr = array(
        "name"=>$name,
        "email"=>$mail,
        "tel"=>$tel,
        "description"=>$description,
        "type_tab"=>'Индивидуальный'
    );
    $DB->Insert('individual_orders',$giveindividarr);

    echo json_encode($giveindividarr);
    session_start();
    $_SESSION['noticemail'] = $mail;
    $_SESSION['notice'] = "Индивидуальный заказ";
    $_SESSION['tzindivinfo'] = " Ф.И.О. заказчика " .$name. ", контактный телефон: " .$tel. ". Описание заказа, оставленное в приложении: " .$description.".";
    require_once "SendMailToSeverStal.php";
}

if(strnatcasecmp($command, "propose") == 0){
    $company_name = $_POST["company_name"];
    $company_type = $_POST["company_type"];
    $name = $_POST["name"];
    $tel = $_POST["tel"];
    $country = $_POST["country"];
    $city = $_POST["city"];
    $mail = $_POST["mail"];
    $site =$_POST["site"];
    $company_type_bd = 0;
    if($company_type == 0){
        $company_type_bd = "Проектное бюро";
    }
    elseif ($company_type == 1) {
        $company_type_bd = "Строительная фирма";
    }
    elseif ($company_type == 2){
        $company_type_bd = "Завод по изготовлению металлоконструкций";
    }

    require_once("../admin/core.php");
    $DB = new DB;
    $DB->connect();
    $proposearr = array(
        "company_name"=>$company_name,
        "company_type"=>$company_type_bd,
        "name"=>$name,
        "tel"=>$tel,
        "country"=>$country,
        "city"=>$city,
        "email"=>$mail,
        "site"=>$site,
        "type_tab"=>'Партнерство'
    );
    $DB->Insert('propose',$proposearr);

    json_encode($proposearr);

    session_start();
    $_SESSION['noticemail'] = $mail;
    $_SESSION['notice'] = "Партнерство";
    $_SESSION['proposeinfo'] = " Контактное лицо в заявке на партнерство - ".$name.", телефон: ".$tel.". ".$company_type_bd. " '".$company_name."', который находится в: ".$country.", ".$city.". Сайт предприятия - ".$site.".";
    require_once "SendMailToSeverStal.php";
}