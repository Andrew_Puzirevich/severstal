<?php
/**
 * Created by PhpStorm.
 * User: Андрій Пузиревич
 * Date: 17.01.16
 * Time: 18:45
 */
session_start();
$notice = $_SESSION['notice'];
$mail = $_SESSION['noticemail'];
$tzinfo =  $_SESSION['tzinfo'];
$tzindivinfo = $_SESSION['tzindivinfo'];
$proposeinfo = $_SESSION['proposeinfo'];

// заголовок письма
$headers= "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=utf-8\r\n"; // кодировка письма
$headers .= "From :CRM Sever Stal <".$mail.">"; // от кого письмо
$arrtext = "Пришел новый заказ в разделе ";
$arrtext .= $notice;
$arrtext .= ". Заявка пришла от ";
$arrtext .= $mail;

if($notice == "Есть готовый проект")
    $arrtext .= $tzinfo;
elseif ($notice == "Индивидуальный заказ")
    $arrtext .= $tzindivinfo;
elseif ($notice == "Партнерство")
    $arrtext .= $proposeinfo;

echo $arrtext;
$arrtext .= "<br><a href='http://sever-stal.bpst.com.ua/admin/site.php'>Перейти к входящей почте</a>";
$result =  mail('andrushkahaker@e-mail.ua', 'Уведомление о новом заказе', $arrtext, $headers); // отправляем письмо
// $result =  $mailSMTP->send('Кому письмо', 'Тема письма', 'Текст письма', 'Заголовки письма');
$arr = array();
$arr['sending']=true;
$arr2 = array();
$arr2['sending']=false;

if($result === true){
    echo json_encode($arr);
}else{
    echo json_encode($arr2);
}