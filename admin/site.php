<?php//if($_SESSION['user'] != "admin"):
//require_once ('../admin/index.php');
//endif;
    ?>

<?php// if($_SESSION['user'] == "admin"):?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8"/>
    <title>BPST CRM Sever-Steel</title>


    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="../css/bootstrap-theme.css" type="text/css">
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css" type="text/css">
    <link rel="stylesheet" href="../css/layout.css" type="text/css" media="screen" />
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="../css/ie.css" type="text/css" media="screen" />
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="../ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="../js/bootstrap.js" type="text/javascript"></script>
    <script src="../js/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="../js/hideshow.js" type="text/javascript"></script>
    <script src="../js/jquery.tablesorter.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.equalHeight.js"></script>
    <script type="text/javascript" src="../js/navigation.js"></script>
    <script type="text/javascript">
        $(document).ready(function()
            {
                $(".tablesorter").tablesorter();
            }
        );
        $(document).ready(function() {
            $(".tab_content").hide();
            $("ul.tabs li:first").addClass("active").show(); //Activate first tab
            $(".tab_content:first").show(); //Show first tab content


            $("ul.tabs li").click(function() {

                $("ul.tabs li").removeClass("active"); //Remove any "active" class
                $(this).addClass("active"); //Add "active" class to selected tab
                $(".tab_content").hide(); //Hide all tab content

                var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
                $(activeTab).fadeIn(); //Fade in the active ID content
                return false;
            });

        });
    </script>
    <script type="text/javascript">
        $(function(){
            $('.column').equalHeight();
        });
    </script>

</head>


<body>

<header id="header">
    <hgroup>
        <h1 class="site_title"><a href="#slidemain" onclick="mainslide()">BPST CRM</a></h1>
        <h2 class="section_title">Север-Сталь Конструкции</h2><div class="btn_view_site"><a href="http://sever-stal.bpst.com.ua/">На сайт</a></div>
    </hgroup>
</header> <!-- end of header bar -->

<section id="secondary_bar">
    <div class="user">
        <p>Администратор</p>
        <!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
    </div>
    <div class="breadcrumbs_container">
        <article class="breadcrumbs"><a>Админка</a> <div class="breadcrumb_divider"></div> <a class="current">Север-Сталь Конструкции</a></article>
    </div>
</section><!-- end of secondary bar -->

<aside id="sidebar" class="column">
    <!--<form class="quick_search">
        <input type="text" value="Быстрый поиск" onfocus="if(!this._haschanged){this.value=''};this._haschanged=true;">
    </form>
    <hr/>-->
    <h3>Письма</h3>
    <ul class="toggle">
        <li class="icn_new_article"><a href="#sendmail" onclick="sendmail()">Напоминания</a></li>
        <li class="icn_edit_article"><a href="#mailonsend" onclick="mailonsend()">Есть готовый проект</a></li>
        <li class="icn_profile"><a href="#slidemain" onclick="mainslide()">Индивидуальный проект</a></li>
        <li class="icn_propose"><a href="#activuser"onclick="activuser()" >Партнерство</a></li>
    </ul>
    <h3>Пользователи</h3>
    <ul class="toggle">
        <li class="icn_add_user"><a href="#adduser" onclick="adduser()">Добавить пользователя</a></li>
        <li class="icn_view_users"><a href="#viewuser" onclick="viewuser()">Пользователи</a></li>
        <!--<li class="icn_profile"><a href="#activuser"onclick="activuser()" >Ваш профиль</a></li>-->
    </ul>
    <h3>База данных</h3>
    <ul class="toggle">
        <li class="icn_folder"><a href="#viewdatabase" onclick="viewdatabase()">Изменить цену за 1т.</a></li>
        <li class="icn_view_users"><a href="#contacts" onclick="contacts()">Контакты</a></li>
    </ul>
    <h3>Admin</h3>
    <ul class="toggle">
        <li class="icn_jump_back"><a href="/admin/index.php">Выйти</a></li>
    </ul>

    <footer>
        <hr />
        <p><strong>Copyright &copy; 2016 BPST Admin</strong></p>
    </footer>
</aside><!-- end of sidebar -->

<section id="main" class="column">



<div id="slidemain" data-slide="slidemain" style="display: none"><!-- main table -->
<article class="module width_full">
    <header><h3>Индивидуальные проекты</h3></header>

   <div class="module_content col-md-10" id="sidebar">
       <ul class="toggle" style="cursor: pointer">
           <li ><button class="btn btn-info btn-block" onclick="allgt()">Новые письма на отправку</button></li>
           <li style="margin-bottom: 10px; margin-top: 10px;"><button class="btn btn-info btn-block"  onclick="allio()">Отправленные письма</button></li>

       </ul>
   </div>

    <div class="module_content width_full" id="allgt">

        <table class="table table-striped">
            <thead>
            <th>№</th>
            <th>Ф.И.О.</th>
            <th>Контактный тел</th>
            <th>E-Mail</th>
            <th>Описание</th>
            <th>Письмо</th>
            </thead>
            <?php
                                require_once('../admin/core.php');
                                $DB = new DB;
                                $DB->connect();
                                $row = $DB->Query("SELECT * FROM individual_orders WHERE sendtrue=0 ORDER BY id DESC");
                                $itteral = 1;
                                foreach($row as $value ){
                                    ?>
            <tr>
                <td> <?php echo $itteral++ ?> </td>
                <td> <?php echo $value["name"]; ?> </td>
                <td> <?php echo $value["tel"]; ?> </td>
                <td> <?php echo $value["email"]; ?> </td>
                <td> <?php echo $value["description"]; ?></td>
                <td>
                    <form method="post" action="/admin/mail.php">
                        <input type="text" name="idmail" id="idmail" value="<?php echo $value["id"]; ?>" style="display: none">
                        <input type="text" name="typemail" id="typemail" value="individual_orders" style="display: none">
                        <input type="text" name="sendorno" id="sendorno" value="0" style="display: none">
                        <button type="submit">Отправить</button>
                    </form>
                </td>
            </tr>
            <?php } ?>
        </table>

        <div class="clear"></div>
    </div>

    <div class="module_content width_full" id="allio" style="display: none">

        <table class="table table-striped">
            <thead>
            <th>№</th>
            <th>Ф.И.О.</th>
            <th>Контактный тел</th>
            <th>E-Mail</th>
            <th>Описание</th>
            <th>Письмо</th>
            </thead>
            <?php
                                    require_once('../admin/core.php');
                                    $DB = new DB;
                                    $DB->connect();
                                    $row = $DB->Query("SELECT * FROM individual_orders WHERE sendtrue!=0 ORDER BY id DESC");
                                    $itteral = 1;
                                    foreach($row as $value ){
                                        ?>
            <tr>
                <td> <?php echo $itteral++ ?> </td>
                <td> <?php echo $value["name"]; ?> </td>
                <td> <?php echo $value["tel"]; ?> </td>
                <td> <?php echo $value["email"]; ?> </td>
                <td> <?php echo $value["description"]; ?></td>
                <td>
                    <form method="post" action="/admin/mail.php">
                        <input type="text" name="idmail" id="idmail" value="<?php echo $value["id"]; ?>" style="display: none">
                        <input type="text" name="typemail" id="typemail" value="individual_orders" style="display: none">
                        <input type="text" name="sendorno" id="sendorno" value="1" style="display: none">
                        <button type="submit">Просмотреть</button>
                    </form>
                </td>
            </tr>
            <?php } ?>
        </table>

        <div class="clear"></div>
    </div>


<div class="clear"></div>
</article>
</div><!-- end main table -->


    <div id="sendmail" style="display: block"> <!--напоминания-->



        <article class="module width_full" >
            <header><h3>Напоминания</h3></header>

            <div class="module_content col-md-11" id="sendmo" style="display: block">

                    <table class="table table-striped">
                        <thead>
                        <th>№</th>
                        <th>E-Mail</th>
                        <th>Письмо</th>
                        </thead>
                        <?php
                        require_once('../admin/core.php');
                        $DB = new DB;
                        $DB->connect();
                        $row = $DB->Query("SELECT * FROM mail_order WHERE sendtrue=1 ORDER BY id DESC");
                        $itteral = 1;
                        foreach($row as $value ){
                            ?>
                            <tr>
                                <td> <?php echo $itteral++ ?> </td>
                                <td> <?php echo $value["email"]; ?> </td>
                                <td>
                                    <form method="post" action="/admin/mail.php">
                                        <input type="text" name="idmail" id="idmail" value="<?php echo $value["id"]; ?>" style="display: none">
                                        <input type="text" name="typemail" id="typemail" value="mail_order" style="display: none">
                                        <input type="text" name="sendorno" id="sendorno" value="1" style="display: none">
                                        <button type="submit">Просмотреть</button>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>


            </div>
            <div class="clear"></div>
        </article>
    </div>

    <div id="mailonsend" style="display: none">

        <article class="module width_full" >
            <header><h3>Есть готовый проект</h3></header>

            <div class="module_content col-md-10" id="sidebar">
                <ul class="toggle" style="cursor: pointer">
                    <li ><button class="btn btn-info btn-block" onclick="onsendgt()">Новые письма на отправку</button></li>
                    <li style=" margin-top: 10px;"><button class="btn btn-info btn-block" onclick="sendgt()">Отправленные письма</button></li>

                </ul>
            </div>



            <div class="module_content width_full" id="onsendgt">

                <table class="table table-striped">
                    <thead>
                    <th>№</th>
                    <th>Ф.И.О.</th>
                    <th>Контактный тел</th>
                    <th>E-Mail</th>
                    <th>Письмо</th>
                    </thead>
                    <?php
                    require_once('../admin/core.php');
                    $DB = new DB;
                    $DB->connect();
                    $row = $DB->Query("SELECT * FROM give_task WHERE sendtrue=0 ORDER BY id DESC");
                    $itteral = 1;
                    foreach($row as $value ){
                        ?>
                        <tr>
                            <td> <?php echo $itteral++ ?> </td>
                            <td> <?php echo $value["name"]; ?> </td>
                            <td> <?php echo $value["tel"]; ?> </td>
                            <td> <?php echo $value["email"]; ?> </td>
                            <td>
                                <form method="post" action="/admin/mail.php">
                                    <input type="text" name="idmail" id="idmail" value="<?php echo $value["id"]; ?>" style="display: none">
                                    <input type="text" name="typemail" id="typemail" value="give_task" style="display: none">
                                    <input type="text" name="sendorno" id="sendorno" value="0" style="display: none">
                                    <button type="submit">Отправить</button>
                                </form>
                            </td>
                        </tr>
                    <?php } ?>
                </table><div class="clear"></div>
            </div>


            <div class="module_content width_full" id="sendgt" style="display: none">

                <table class="table table-striped">
                    <thead>
                    <th>№</th>
                    <th>Ф.И.О.</th>
                    <th>Контактный тел</th>
                    <th>E-Mail</th>
                    <th>Письмо</th>
                    </thead>
                    <?php
                    require_once('../admin/core.php');
                    $DB = new DB;
                    $DB->connect();
                    $row = $DB->Query("SELECT * FROM give_task WHERE sendtrue!=0 ORDER BY id DESC");
                    $itteral = 1;
                    foreach($row as $value ){
                        ?>
                        <tr>
                            <td> <?php echo $itteral++ ?> </td>
                            <td> <?php echo $value["name"]; ?> </td>
                            <td> <?php echo $value["tel"]; ?> </td>
                            <td> <?php echo $value["email"]; ?> </td>
                            <td>
                                <form method="post" action="/admin/mail.php">
                                    <input type="text" name="idmail" id="idmail" value="<?php echo $value["id"]; ?>" style="display: none">
                                    <input type="text" name="typemail" id="typemail" value="give_task" style="display: none">
                                    <input type="text" name="sendorno" id="sendorno" value="1" style="display: none">
                                    <button type="submit">Просмотреть</button>
                                </form>
                            </td>
                        </tr>
                    <?php } ?>
                </table>


            </div>
            <div class="clear"></div>

        </article>
    </div>






    <div id="activuser" style="display: none;">

        <article class="module width_full" >
            <header><h3>Заявки в партнерской программе:</h3></header>

            <div class="module_content col-md-10 " id="sidebar">
                <ul class="toggle" style="cursor: pointer">
                    <li ><button class="btn btn-info btn-block" onclick="onsendpropose()">Новые письма на отправку</button></li>
                    <li style=" margin-top: 10px;"><button class="btn btn-info btn-block" onclick="sendpropose()">Отправленные письма</button></li>

                </ul>
            </div>



            <div class="module_content width_full" id="onsendpropose">

                <table class="table table-striped">
                    <thead>
                    <th>№</th>
                    <th>Компания</th>
                    <th>Тип производства</th>
                    <th>Страна</th>
                    <th>Город</th>
                    <th>Контактное лицо</th>
                    <th>Контактный тел</th>
                    <th>E-Mail</th>
                    <th>Сайт</th>
                    <th>Письмо</th>
                    </thead>
                    <?php
                                        require_once('../admin/core.php');
                                        $DB = new DB;
                                        $DB->connect();
                                        $row = $DB->Query("SELECT * FROM propose WHERE sendtrue=0 ORDER BY id DESC");
                                        $itteral = 1;
                                        foreach($row as $value ){
                                            ?>
                    <tr>
                        <td> <?php echo $itteral++ ?> </td>
                        <td><? echo $value["company_name"] ?></td>
                        <td><? echo $value["company_type"] ?></td>
                        <td><? echo $value["country"] ?></td>
                        <td><? echo $value["city"] ?></td>
                        <td> <?php echo $value["name"]; ?> </td>
                        <td> <?php echo $value["tel"]; ?> </td>
                        <td> <?php echo $value["email"]; ?> </td>
                        <td><? echo $value["site"] ?></td>
                        <td>
                            <form method="post" action="/admin/mail.php">
                                <input type="text" name="idmail" id="idmail" value="<?php echo $value["id"]; ?>" style="display: none">
                                <input type="text" name="typemail" id="typemail" value="propose" style="display: none">
                                <input type="text" name="sendorno" id="sendorno" value="0" style="display: none">
                                <button type="submit">Отправить</button>
                            </form>
                        </td>
                    </tr>
                    <?php } ?>
                </table><div class="clear"></div>
            </div>


            <div class="module_content width_full" id="sendpropose" style="display: none">

                <table class="table table-striped">
                    <thead>
                    <th>№</th>
                    <th>Ф.И.О.</th>
                    <th>Контактный тел</th>
                    <th>E-Mail</th>
                    <th>Письмо</th>
                    </thead>
<?php
require_once('../admin/core.php');
$DB = new DB;
$DB->connect();
$row = $DB->Query("SELECT * FROM propose WHERE sendtrue!=0 ORDER BY id DESC");
$itteral = 1;
foreach($row as $value ){
    ?>
    <tr>
        <td> <?php echo $itteral++ ?> </td>
        <td><?php echo $value["company_name"] ?></td>
        <td><?php echo $value["company_type"] ?></td>
        <td><?php echo $value["country"] ?></td>
        <td><?php echo $value["city"] ?></td>
        <td> <?php echo $value["name"]; ?> </td>
        <td> <?php echo $value["tel"]; ?> </td>
        <td> <?php echo $value["email"]; ?> </td>
        <td><?php echo $value["site"] ?></td>
        <td>
                                <form method="post" action="/admin/mail.php">
                                    <input type="text" name="idmail" id="idmail" value="<?php echo $value["id"]; ?>" style="display: none">
                                    <input type="text" name="typemail" id="typemail" value="propose" style="display: none">
                                    <input type="text" name="sendorno" id="sendorno" value="1" style="display: none">
                                    <button type="submit">Просмотреть</button>
                                </form>
                            </td>
                        </tr>
                    <?php } ?>
                </table>


            </div>
            <div class="clear"></div>

        </article>

    <div class="clear"></div>
</div>
<!-- end mail block -->
    <div id="adduser" style="display: none;">

        <?php

        require_once('auth.php');
        require_once('../admin/core.php');
        $DB = new DB;
        $DB->connect();
       if ((isset($_POST['mail'])) && (isset($_POST['log'])) && (isset($_POST['pas'])))
       {
           if((empty($_POST['mail'])) || (empty($_POST['log'])) || (empty($_POST['pas'])))
           {
               echo 'Введите все поля в форму';
           }
           else
            $arr = array("login"=>$_POST['log'],"pass"=>$_POST['pas'],"mail"=>$_POST['mail']);
            $DB->Insert('users',$arr);
       }
       ?>
        <div class="clear"></div>
    </div>

    <div id="viewuser"style="display: none;">

        <article class="module width_full" >
            <header><h3>Пользователи на сайте:</h3></header>
            <div class="module_content">

                <article>
                    <table class="table table-striped">
                        <thead>
                        <th>№</th>
                        <th>Логин</th>
                        <th>Пароль</th>
                        <th>E-Mail</th>
                        <th>Редактировать</th>
                        </thead>
                        <?php
                        require_once('../admin/core.php');
                        $DB = new DB;
                        $DB->connect();
                        $row = $DB->Query("SELECT * FROM users");
                        $itteral=1;
                        foreach($row as $value ){
                            ?>
                            <tr>

                                <td> <?php echo $itteral++ ?> </td>
                                <td> <?php echo $value["login"]; ?> </td>
                                <td> <?php echo $value["pass"]; ?> </td>
                                <td> <?php echo $value["mail"]; ?> </td>
                                <td><a href="#">open & send</a> </td>
                            </tr>
                        <?php } ?>
                    </table>
                </article>
                <div class="clear"></div>
            </div>
        </article>

        <div class="clear"></div>
    </div>


<!-- end users block -->
    <div  id="viewdatabase"style="display: none;">
        <?php
        require_once('addprice.php');
        require_once('../admin/core.php');
        $DB = new DB;
        $DB->connect();
        if(isset($_POST['price']))
        {
            if(!empty($_POST['price']))
            {
            $arr =  array(
                "price"=>$_POST['price']);
            $DB->update('prices',$arr,'id',1);
            } else echo'Enter price';
        }
        ?>
        <div class="clear"></div>
    </div>

    <div id="contacts"style="display: none;">

        <article class="module width_full" >
            <header><h3>Список контактов:</h3></header>
            <div class="module_content">

                <article>
                    <table class="table table-striped">
                        <thead>
                        <th>№</th>
                        <th>Ф.И.О.</th>
                        <th>E-Mail</th>
                        <th>Телефон</th>
                        <th>Компания</th>
                        <th>От куда</th>
                        </thead>
                        <?php
                        require_once('../admin/core.php');
                        $DB = new DB;
                        $DB->connect();
                        $row = $DB->Query("SELECT DISTINCT email,name,tel,type_tab FROM give_task");
                        $row2 = $DB->Query("SELECT DISTINCT email,name,tel,type_tab FROM individual_orders");
                        $row3 = $DB->Query("SELECT DISTINCT email,type_tab FROM mail_order");
                        $row4 = $DB->Query("SELECT DISTINCT email,company_name,name,tel,type_tab FROM propose");
                        $result = array_merge($row,$row2,$row3,$row4);
                        $itteral=1;
                        foreach($result as $value ){
                            ?>
                            <tr>

                                <td> <?php echo $itteral++ ?> </td>
                                <td> <?php echo $value["name"]; ?> </td>
                                <td> <?php echo $value["email"]; ?> </td>
                                <td> <?php echo $value["tel"]; ?> </td>
                                <td> <?php echo $value["company_name"]; ?> </td>
                                <td> <?php echo $value["type_tab"]; ?> </td>
                            </tr>
                        <?php } ?>
                    </table>
                </article>
                <div class="clear"></div>
            </div>
        </article>

        <div class="clear"></div>
    </div>
<!-- end database block -->

<!--mail-->

    <div id="mailblock" style="display: none;">
        <?php
        require_once('/admin/mail.php');
        ?>
    </div>
<div class="spacer"></div>
</section>


</body>

</html>
<?// endif; ?>