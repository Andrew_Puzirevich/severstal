<?php
/**
 * Created by PhpStorm.
 * User: Андрій Пузиревич
 * Date: 22.02.16
 * Time: 8:38
 */
require_once('../admin/core.php');
$DB = new DB;
$DB->connect();
$row = $DB->Query("SELECT * FROM prices");
foreach($row as $value ){
?>
<article class="module width_full">
    <div>

        <header><h3>Цена на металл:</h3></header>
        <h3> В данный момент, цена за 1 тонну металла становит: <?php echo $value['price'];   ?>  тисяч гривен.</h3><?php }?>
    </div>

    <form method="post" action="site.php">
        <div class="form-group">
            <label for="price" class="col-sm-5 control-label">Введите в поле новую цену, в формате "00.0"</label>
            <input type="text" id="price" name="price" class="col-sm-1">
        </div>
        <div class="form-group">
            <button type="submit" name="bt" class="btn btn-info center-block">Изменить цену</button>
        </div>
    </form>
    <div class="clear"></div>
</article>